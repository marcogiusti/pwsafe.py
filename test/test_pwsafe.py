# Copyright (c) 2018 Marco Giusti

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import datetime
import io
from os.path import abspath, dirname, join as joinpath
import unittest
import uuid

import pwsafe
from pwsafe import (
    PwsafeV3Reader, PwsafeV3Writer, Field, Version, END, UUID, Title,
    Username, Password, Url, CreationTime, End, TextField, IntField,
    RawField, CorruptedFile, DigestError, Note
)


utc = datetime.timezone.utc
test_dir = abspath(dirname(__file__))


class TestFields(unittest.TestCase):

    def test_str(self):
        self.assertTrue(issubclass(Username, Field))
        self.assertTrue(issubclass(Username, str))

    def test_str_serialize(self):
        expected = bytes.fromhex('6d6172636f')
        self.assertEqual(Username('marco').to_bytes(), expected)

    def test_str_repr(self):
        self.assertEqual(repr(Username('marco')), "Username('marco')")

    def test_uuid(self):
        self.assertTrue(issubclass(UUID, Field))
        self.assertTrue(issubclass(UUID, uuid.UUID))

    def test_uuid_serialize(self):
        exp = bytes.fromhex('95675c664da54513b3c36229091c85bf')
        uid = UUID('665c6795-a54d-1345-b3c3-6229091c85bf')
        self.assertEqual(uid.to_bytes(), exp)

    def test_uuid_repr(self):
        uuid = UUID('665c6795-a54d-1345-b3c3-6229091c85bf')
        self.assertEqual(
            repr(uuid),
            "UUID('665c6795-a54d-1345-b3c3-6229091c85bf')"
        )

    def test_int(self):
        self.assertTrue(issubclass(Version, Field))
        self.assertTrue(issubclass(Version, int))

    def test_int_serialize(self):
        expected = bytes.fromhex('0003')
        self.assertEqual(Version(768).to_bytes(), expected)

    def test_end(self):
        self.assertIs(End.from_bytes(b'~~noise~~'), END)
        self.assertEqual(repr(END), 'End()')

    def test_end_serialize(self):
        self.assertEqual(END.to_bytes(), b'')

    def test_subclass(self):
        MyField = TextField._subclass('MyField', 0x0c)
        self.assertTrue(issubclass(MyField, Field))
        self.assertTrue(issubclass(MyField, str))

    def test_int_subclass(self):
        MyIntField = IntField._subclass('MyIntField', 0x0c, 4)
        self.assertTrue(issubclass(MyIntField, Field))
        self.assertTrue(issubclass(MyIntField, int))

    def test_raw(self):
        BYTES = b'abc'
        field = RawField.from_bytes(BYTES)
        self.assertEqual(field, BYTES)
        self.assertEqual(field.to_bytes(), field)


class TestTimeField(unittest.TestCase):

    def test_default_timezone(self):
        '''
        Default timezone is UTC.
        '''

        self.assertIs(CreationTime(2019, 8, 15).tzinfo, utc)

    def test_from_datetime_naive(self):
        '''
        Create a TimeField instance from a naive datetime.datetime
        object.
        '''

        dt = datetime.datetime(2019, 8, 15)
        ctime = CreationTime.from_datetime(dt)
        self.assertIsInstance(ctime, CreationTime)
        self.assertIs(ctime.tzinfo, utc)

    def test_from_datetime_utc(self):
        '''
        Create a TimeField instance from a datetime.datetime object.
        '''

        dt = datetime.datetime(2019, 8, 15, 20, 22, tzinfo=utc)
        ctime = CreationTime.from_datetime(dt)
        self.assertIsInstance(ctime, CreationTime)
        self.assertIs(ctime.tzinfo, utc)
        self.assertEqual(ctime, dt)

    def test_from_datetime(self):
        '''
        Create a TimeField instance from a datetime.datetime object with
        a different timezone.
        '''

        tz = datetime.timezone(datetime.timedelta(hours=1))
        dt = datetime.datetime(2019, 8, 15, 20, 22, tzinfo=tz)
        ctime = CreationTime.from_datetime(dt)
        self.assertIsInstance(ctime, CreationTime)
        self.assertEqual(ctime.tzinfo, tz)
        self.assertEqual(ctime, dt)

    def test_deserialization(self):
        BYTES = b'H)\x1aY'
        ctime = CreationTime.from_bytes(BYTES)
        self.assertIsInstance(ctime, CreationTime)
        expected = CreationTime(2017, 5, 15, 22, 18, 48, tzinfo=utc)
        self.assertEqual(ctime, expected)

    def test_serialization(self):
        tz = datetime.timezone(datetime.timedelta(hours=1))
        dt = datetime.datetime(2019, 8, 15, 20, 22, tzinfo=tz)
        ctime = CreationTime.from_datetime(dt)
        self.assertIs(ctime.tzinfo, tz)
        utc_ctime = CreationTime(2019, 8, 15, 19, 22)
        self.assertEqual(ctime.to_bytes(), utc_ctime.to_bytes())

    def test_repr(self):
        ctime = CreationTime(2019, 8, 15, 20, 22)
        self.assertEqual(
            repr(ctime),
            'CreationTime(2019, 8, 15, 20, 22, tzinfo=datetime.timezone.utc)'
        )

    def assert_times_almost_equal(self, time1, time2):
        # because a simple comparison can spuriously fail, check the
        # time delta instead
        self.assertLessEqual((time1 - time2).seconds, 1)

    def test_now(self):
        cest = datetime.timezone(datetime.timedelta(hours=+2))
        utc_time = CreationTime.now()
        cest_time = CreationTime.now(cest)
        # because a simple comparison can spuriously fail, check the
        # time delta instead
        self.assert_times_almost_equal(utc_time, cest_time)

    def test_utc_now(self):
        utc_time = CreationTime.now()
        utc_time2 = CreationTime.utcnow()
        # because a simple comparison can spuriously fail, check the
        # time delta instead
        self.assert_times_almost_equal(utc_time, utc_time2)


class TestReadWrite(unittest.TestCase):

    NO_DB = joinpath(test_dir, 'zero.psafe3')
    EMPTY_DB = joinpath(test_dir, 'empty.psafe3')
    TEST_DB = joinpath(test_dir, 'test.psafe3')
    WRONG_TAG_DB = joinpath(test_dir, 'wrong_tag.psafe3')
    BIG_FIELD_DB = joinpath(test_dir, 'big_field.psafe3')
    UNKNOWN_FIELD_DB = joinpath(test_dir, 'unknown_field.psafe3')
    PWD = b'test'

    def test_is_pwsafe(self):
        self.assertTrue(PwsafeV3Reader.is_pwsafe(self.EMPTY_DB))

    def test_is_no_pwsafev3(self):
        self.assertFalse(PwsafeV3Reader.is_pwsafe(self.NO_DB))
        self.assertFalse(PwsafeV3Reader.is_pwsafe(self.WRONG_TAG_DB))

    def test_empty_db(self):
        with PwsafeV3Reader.open(self.EMPTY_DB, self.PWD) as dbfile:
            header, *records = list(dbfile)
            self.assertEqual(header, [0x300, UUID(int=0), 'B 24 1'])
            self.assertEqual(records, [])

    def test_is_pwsafe_low_iterations(self):

        def restore_min_hash_iterations(num):
            pwsafe.MIN_HASH_ITERATIONS = num

        with open(self.TEST_DB, 'rb') as fp:
            header = pwsafe._Header.from_file(fp)
        self.addCleanup(
            restore_min_hash_iterations, pwsafe.MIN_HASH_ITERATIONS
        )
        pwsafe.MIN_HASH_ITERATIONS = header.iterations + 1
        self.assertTrue(PwsafeV3Reader.is_pwsafe(self.TEST_DB))

    def test_db(self):
        utc = datetime.timezone.utc
        with PwsafeV3Reader.open(self.TEST_DB, self.PWD) as dbfile:
            header, r1, r2 = list(dbfile)
            self.assertEqual(header, [0x300, UUID(int=0), 'B 24 1'])
            self.assertEqual(
                r1,
                [
                    uuid.UUID('665c6795-a54d-1345-b3c3-6229091c85bf'),
                    'test',
                    'marco',
                    'some notes',
                    'qwerty',
                    datetime.datetime(2017, 5, 7, 7, 20, 23, tzinfo=utc),
                    datetime.datetime(2017, 5, 7, 7, 20, 23, tzinfo=utc),
                    'http://www.example.com/'
                ]
            )
            self.assertEqual(
                r2,
                [
                    uuid.UUID('12084a3c-d97d-2346-a089-322777c3e016'),
                    'group1.group2.group3',
                    'test nested',
                    'marco',
                    'qwerty',
                    datetime.datetime(2017, 5, 7, 7, 21, 33, tzinfo=utc),
                    datetime.datetime(2017, 5, 7, 7, 21, 33, tzinfo=utc)
                ]
            )

    def test_write(self):
        fp = io.BytesIO()
        KEY = b'secret'
        writer = PwsafeV3Writer(fp, KEY)
        header = [Version(3), UUID.new()]
        writer.write_record(header)
        uuid = UUID.new()
        title = Title('Github account')
        url = Url('https://github.com')
        username = Username('marcogiusti')
        password = Password('qwerty')
        record = [uuid, title, url, username, password]
        writer.write_record(record)
        writer.close()
        fp.seek(0)
        reader = PwsafeV3Reader(fp, KEY)
        expected_header, *records = list(reader)
        self.assertEqual(header, expected_header)
        self.assertEqual(records, [record])

    def test_premature_eof(self):
        '''
        If for some reason we cannot read a block or EOF is not found,
        CorruptedFile exception is raised.
        '''

        with open(self.TEST_DB, 'rb') as fp:
            data = fp.read()
        # trim HMAC and EOF
        data = data[:len(data) - 16 - 32]
        db = PwsafeV3Reader(io.BytesIO(data), self.PWD)
        self.assertRaises(CorruptedFile, list, db)

    def test_no_hmac(self):
        '''
        If HMAC is missing, DigestError is raised.
        '''

        with open(self.TEST_DB, 'rb') as fp:
            data = fp.read()
        # trim HMAC
        data = data[:len(data) - 16]
        db = PwsafeV3Reader(io.BytesIO(data), self.PWD)
        self.assertRaises(DigestError, list, db)

    def test_big_field(self):
        with PwsafeV3Reader.open(self.BIG_FIELD_DB, self.PWD) as db:
            with self.assertRaises(pwsafe.Error) as cm:
                list(db)
        self.assertRegex(
            str(cm.exception),
            r'field length \d+ looks insane'
        )
        with PwsafeV3Reader.open(self.BIG_FIELD_DB, self.PWD) as db:
            db.MAX_FIELD_SIZE = 65537
            # This will not raise
            list(db)

    def test_unknown_field(self):
        with PwsafeV3Reader.open(self.UNKNOWN_FIELD_DB, self.PWD) as db:
            _, record = db
        self.assertIsInstance(record[-1], RawField)
        self.assertEqual(record[-1].type_id, 0xdf)

    def test_end_field_not_last(self):
        record = [
            UUID.new(), Title('test'), Password('querty'), END,
            Note('this will be lost')
        ]
        fp = io.BytesIO()
        PWD = b'test'
        writer = PwsafeV3Writer(fp, PWD)
        writer.write_record([Version(3), UUID.new()])
        writer.write_record(record)
        writer.close()
        fp.seek(0)
        reader = PwsafeV3Reader(fp, PWD)
        _, record2 = reader
        self.assertEqual(record2, record[:3])
