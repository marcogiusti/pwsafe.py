# Copyright (c) 2018 Marco Giusti
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import os
import abc
import datetime
import hashlib
import hmac
import struct
import sys
import uuid
from collections import namedtuple

import twofish


__version__ = '0.3.0'
__author__ = 'Marco Giusti'
__license__ = 'MIT'
__all__ = (
    # high level interface
    'PwsafeV3Reader', 'PwsafeV3Writer', 'Error', 'NotPwsafeV3',
    'InvalidPassword', 'DigestError',
    # Header Fields
    'Version', 'UUID', 'NonDefaultPreferences', 'TreeDisplayStatus',
    'LastSave', 'WhoLastSave', 'WhatLastSave', 'LastSavedByUser',
    'LastSavedOnHost', 'DatabaseName', 'DatabaseDescription',
    'DatabaseFilters', 'RecentlyUsedEntries', 'NamedPasswordPolicies',
    'EmptyGroups', 'Yubico', 'End',
    # Record fields
    'Group', 'Title', 'Username', 'Note', 'Password', 'CreationTime',
    'PasswordModificationTime', 'LastAccessTime', 'PasswordExpiryTime',
    'LastModificationTime', 'Url', 'Autotype', 'PasswordHistory',
    'PasswordPolicy', 'PasswordExpiryInterval', 'RunCommand',
    'DoubleClickAction', 'EmailAddress', 'ProtectedEntry',
    'OwnSymbolsPassword', 'ShiftDoubleClickAction', 'EntryKeyboardShortcut',
    'TwoFactorKey', 'CreditCardNumber', 'CreditCardExpiration',
    'CreditCardVerification', 'CreditCardPin', 'QRCode',
    'PasswordPolicyName', 'END',
    # Miscellaneous
    'Field', 'RawField', 'IntField', 'TextField', 'TimeField', 'xor_bytes'
)

TAG = b'PWS3'
EOF = b'PWS3-EOFPWS3-EOF'  # 16 bytes
DIGESTMOD = hashlib.sha256
MIN_HASH_ITERATIONS = 2 ** 11


class Error(Exception):
    pass


class NotPwsafeV3(Error):
    pass


class InvalidPassword(Error):
    pass


class CorruptedFile(Error):
    pass


class DigestError(CorruptedFile):
    pass


class InsecureError(Error):
    '''
    Raised if the number of iterations used to stretch the key is less
    than the minimun. Note that you can still change the minimum number
    of iteration to accept databases with with all number of iterations.
    '''


class _EOF(Exception):
    pass


def stretch_key(key, salt, iterations, _hash=hashlib.sha256):
    '''
    Stretch the key following the algorithm from the paper "Secure
    Applications of Low-Entropy Keys",
    https://www.schneier.com/academic/paperfiles/paper-low-entropy.pdf

    :param bytes key: the key to stretch.
    :param bytes salt: the salt for the key.
    :param int iterations: the number of iteration to perform.
    :rtype bytes:
    '''

    x = _hash(key + salt).digest()
    for i in range(iterations):
        x = _hash(x).digest()
    return x


class _Header(namedtuple('_Header', 'tag salt iterations hp1 b1 b2 b3 b4 iv')):
    '''
    File header. This is a private class and the user never iteracts
    with it.

    It is composed by the following fields:

        - tag: the sequence of 4 ASCII characters "PWS3";
        - salt: a 256 bit random value, generated at file creation time;
        - iterations: the number of iterations on the hash function to
          calculate P';
        - hp1: the SHA-256(P'), used to verify that the user has the
          correct passphrase;
        - b1, b2: two 128-bit blocks encrypted with Twofish using P' as
          the key, in ECB mode. These blocks contain the 256 bit random
          key K that is used to encrypt the actual records;
        - b3, b4: two 128-bit blocks encrypted with Twofish using P' as the
          key, in ECB mode. These blocks contain the 256 bit random key
          L that is used to calculate the HMAC of the encrypted data;
        - iv: the 128-bit random Initial Value for CBC mode.
    '''

    __slots__ = ()
    struct = struct.Struct('<4s32sI32s16s16s16s16s16s')

    @classmethod
    def from_file(cls, fp):
        '''
        Create and instance from a file-type object. NotPwsafeV3 is
        raised in case of error.

        :param file fp:
        :rtype: _Header
        :raise: NotPwsafeV3
        '''

        return cls.from_bytes(fp.read(cls.struct.size))

    @classmethod
    def from_bytes(cls, data):
        '''
        Create and instance from a bytes-type object. NotPwsafeV3 is
        raised in case of error.

        :param bytes data:
        :rtype: _Header
        :raise: NotPwsafeV3
        '''

        try:
            return cls._make(cls.struct.unpack(data))
        except struct.error:
            raise NotPwsafeV3('truncated file')

    def to_bytes(self):
        '''
        Serialize the header to a bytes object. The to_bytes function is
        the inverse of from_bytes:

            _Header.from_bytes(hdr.to_bytes()) == hdr
            _Header.from_bytes(b).to_bytes() == b

        :rtype: bytes
        '''

        return self.struct.pack(*self)


def xor_bytes(b1, b2):
    '''
    XOR two bytes-like objects byte per byte. If the two objects do not
    have the same length, the result will have the same length of the
    longest one.

    :param bytes b1: First bytes object.
    :param bytes b2: Second bytes object.
    :rtype: bytes
    '''

    i1 = int.from_bytes(b1, sys.byteorder)
    i2 = int.from_bytes(b2, sys.byteorder)
    M = max(len(b1), len(b2))
    return (i1 ^ i2).to_bytes(M, sys.byteorder)


class PwsafeV3Reader:
    '''
    Reader for the PasswordSafe file format.

    If the reader is created from the open method, the respective close
    method must be called before dismissing the instance.

    If you want to recover a corrupted database, overriding _read_record
    and, if need be, the __iter__ methods.

    Look [1] for the file format.

    https://github.com/pwsafe/pwsafe/blob/HEAD/docs/formatV3.txt
    '''

    MAX_FIELD_SIZE = 65536
    _should_close = False

    @classmethod
    def is_pwsafe(cls, filename):
        '''
        Check if the given file is a pwsafe file.

        :param str filename: the name of the file to check.
        :rtype: bool
        '''

        try:
            with cls.open(filename, b''):
                pass
        except NotPwsafeV3:
            return False
        except (InvalidPassword, InsecureError):
            pass
        return True

    @classmethod
    def open(cls, filename, key):
        '''
        Create an instance of the reader from the filename. The file
        object will be closed when the close method is called.

        :param str filename: the password database file.
        :param bytes key: the password used to encrypt the database.
        :rtype: PwsafeV3Reader
        :raises: Error
        '''

        fp = open(filename, 'rb')
        try:
            self = cls(fp, key)
        except:  # noqa
            fp.close()
            raise
        self._should_close = True
        return self

    def __init__(self, fp, key):
        '''
        Instance a new reader from a file-like object. The file object
        will be closed only if the PwsafeV3Reader.open function has been
        used.

        :param file fp: the open password database file.
        :param bytes key: the password used to encrypt the database.
        '''

        self._fp = fp
        header = _Header.from_file(fp)
        if header.tag != TAG:
            raise NotPwsafeV3('invalid tag "%s"' % header.tag)
        if header.iterations < MIN_HASH_ITERATIONS:
            raise InsecureError(
                'not enough iterations {} (minimum: {})'.format(
                    header.iterations, MIN_HASH_ITERATIONS
                )
            )
        p1 = stretch_key(key, header.salt, header.iterations)
        if not hmac.compare_digest(hashlib.sha256(p1).digest(), header.hp1):
            raise InvalidPassword()
        ff = twofish.Twofish(p1)
        K = ff.decrypt(header.b1) + ff.decrypt(header.b2)
        L = ff.decrypt(header.b3) + ff.decrypt(header.b4)
        self._hmac = hmac.new(L, digestmod=DIGESTMOD)
        self._fishfish = twofish.Twofish(K)
        self._iv = header.iv

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        self.close()

    def close(self):
        '''
        Close the underline file object if it was open by this instance
        and if the file object was received as parameter.

        Ex.

            reader = PwsafeV3Reader(fp, key)
            reader.close()  # this is no-op

            reader = PwsafeV3Reader.open(filename, key)
            reader.close()  # this will close the underline file object
        '''

        if self._should_close:
            self._fp.close()

    def __iter__(self):
        '''
        Iterate through the records of the database.

        The first returned item is the database header, all the
        following returned items are the records. Records and the header
        are just list of fields. The EOF record is not returned as well
        as the HMAC. Each record miss the END field.

        If the digest check fails, DigestError is raised. It is still
        possible to gather the records explicitely accumulating them
        into a list:

            records = []
            itr = iter(pwsafe_reader)
            while True:
                try:
                    records.append(next(itr))
                except (DigestError, StopIteration):
                    pass
        '''

        # header
        yield list(self._read_record(_HEADERS_MAP))
        while 1:
            try:
                yield list(self._read_record(_RECORDS_MAP))
            except _EOF:
                self._check_digest()
                return

    def _read_record(self, field_types):
        '''
        :rtype: Iterable[Field]
        '''

        field = self._read_field(field_types)
        while field is not END:
            yield field
            field = self._read_field(field_types)

    def _read_field(self, field_types, block_size=16):
        '''
        :rtype: Field
        '''

        first_block = self._read_block(block_size)
        length = int.from_bytes(first_block[:4], 'little')
        if not 0 <= length <= self.MAX_FIELD_SIZE:
            raise Error('field length {} looks insane'.format(length))
        type_id = first_block[4]
        rest = length - (block_size - 5)
        data = first_block[5:length+5]
        while rest > 0:
            data += self._read_block(block_size)[:rest]
            rest -= block_size
        self._hmac.update(data)
        if type_id in field_types:
            return field_types[type_id].from_bytes(data)
        name = 'RawField{}'.format(type_id)
        field_types[type_id] = T = RawField._subclass(name, type_id)
        return T.from_bytes(data)

    def _read_block(self, block_size):
        block = self._fp.read(block_size)
        if block == EOF:
            raise _EOF()
        if len(block) != block_size:
            raise CorruptedFile('End of file while reading field')
        data = xor_bytes(self._fishfish.decrypt(block), self._iv)
        self._iv = block
        return data

    def _check_digest(self):
        digest = self._fp.read(self._hmac.block_size)
        if not hmac.compare_digest(self._hmac.digest(), digest):
            raise DigestError('invalid hmac')


class PwsafeV3Writer:
    '''
    Utility class to create Password Safe compatible password databases.

    After instantiated the class, use the write_record method once for
    each record. Note that the first call to this method should be done
    for the database header. No checks are performed to enforce this.

    After all the records have been written, the close method must be
    called to write the EOF block and calculate the checksum of the
    database.

        >>> import io
        >>> import pwsafe
        >>> fp = io.BytesIO()
        >>> writer = pwsafe.PwsafeV3Writer(fp, b'secret')
        # the header
        >>> writer.write_record([pwsafe.Version(3), pwsafe.UUID.new()])
        >>> uuid = pwsafe.UUID.new()
        >>> title = pwsafe.Title('Github account')
        >>> url = pwsafe.Url('https://github.com')
        >>> username = pwsafe.Username('marcogiusti')
        >>> password = pwsafe.Password('qwerty')
        >>> writer.write_record([uuid, title, username, password])
        >>> writer.close()

    :param file fp: a file-like object. Most preciselly, an object
        implementing io.RawIOBase or io.BufferedIOBase. fp.writable()
        must return True.
    :param bytes key: the database master key.
    :param Optional[int] iterations: the number of iteration used to
        stretch the master key. The number of iterations will never be
        less than the suggested minimum (2048).
    '''

    _eof_written = False

    def __init__(self, fp, key, iterations=None):
        self._fp = fp
        if iterations is None or iterations < MIN_HASH_ITERATIONS:
            iterations = MIN_HASH_ITERATIONS
        salt = os.urandom(32)
        K = os.urandom(32)
        L = os.urandom(32)
        p1 = stretch_key(key, salt, iterations)
        hp1 = hashlib.sha256(p1).digest()
        ff = twofish.Twofish(p1)
        b1 = ff.encrypt(K[:16])
        b2 = ff.encrypt(K[16:])
        b3 = ff.encrypt(L[:16])
        b4 = ff.encrypt(L[16:])
        self._fishfish = twofish.Twofish(K)
        self._hmac = hmac.new(L, digestmod=DIGESTMOD)
        self._iv = iv = os.urandom(16)
        header = _Header(TAG, salt, iterations, hp1, b1, b2, b3, b4, iv)
        fp.write(header.to_bytes())

    def _write_eof(self):
        assert not self._eof_written, 'writer closed'
        self._fp.write(EOF)
        self._fp.write(self._hmac.digest())
        self._eof_written = True

    def close(self):
        '''
        Write the EOF block and calculate the checksum of the database.
        This method must be called once, and only once, after all the
        records have been written into the database.
        '''

        self._write_eof()

    def _write_field(self, field, block_size=16):
        raw = field.to_bytes()
        self._hmac.update(raw)
        length = len(raw)
        padding_length = (block_size - (length + 5) % block_size) % block_size
        padding = os.urandom(padding_length)
        data = (
            length.to_bytes(length=4, byteorder='little') +
            field.type_id.to_bytes(length=1, byteorder='little') +
            raw + padding
        )
        assert len(data) % block_size == 0
        for i in range(len(data) // block_size):
            block = data[i*block_size:(i+1)*block_size]
            enc = self._fishfish.encrypt(xor_bytes(block, self._iv))
            self._fp.write(enc)
            self._iv = enc

    def write_record(self, record):
        '''
        Write a record to the database. A record is an iterable of
        fields. A valid record must contains at least the UUID, the
        Title and the Password fields. It is recommended to include the
        End field, but it is automatically written if missing.
        Implementation note, if the End field is not the last, all the
        following fields are discarded.

        :param Iterable[Field] record: the fields in the record.
        '''

        for field in record:
            self._write_field(field)
            if field is END:
                break
        else:
            self._write_field(END)


class Field(metaclass=abc.ABCMeta):
    '''
    Abstract class for all the field types.

    The interface has one field and two methods:

        - type_id, an integer designating the field type;
        - to_bytes(), serialize the objecto to bytes;
        - from_bytes(), create an instance from the bytes
            representation.

    The concrete subclasses inherit also from other base classes to
    represent one of the following values:

        - UUID
        - text
        - time
        - integer
        - bytes

    There is another subclass, End, that marks the end of a record.
    '''

    __slots__ = ()

    @property
    @abc.abstractmethod
    def type_id(self):
        '''
        The type of this field. The types are described in the
        specifications for the PasswordSafe database format.
        '''

        raise NotImplementedError()  # pragma: nocover

    @abc.abstractmethod
    def to_bytes(self):
        '''
        Serialize the field to bytes.

        :rtype: bytes
        '''

        raise NotImplementedError()  # pragma: nocover

    @classmethod
    @abc.abstractmethod
    def from_bytes(cls, raw):
        '''
        Build an instance of this class starting from the serialized
        form.

        :param bytes raw: a bytes-like object.
        :rtype: Field
        '''

        raise NotImplementedError()  # pragma: nocover

    @classmethod
    def _subclass(cls, name, type_id):
        '''
        Utility function to create a subclass of this class. This will
        help spare some line of code.

        :param str name: the name of the subclass.
        :param int type_id: the new field type.
        :rtype: an Field subclass
        '''

        attrs = {'type_id': type_id, '__slots__': (), '__module__': __name__}
        T = type(name, (cls, ), attrs)
        return T

    def __repr__(self):
        return '{0.__class__.__name__}({1})'.format(self, super().__repr__())


class TextField(Field, str):
    '''
    An abstract Field subclass representing some text.
    '''

    __slots__ = ()

    @classmethod
    def from_bytes(cls, raw):
        return cls(raw, encoding='utf-8')

    def to_bytes(self):
        return self.encode('utf-8')


class TimeField(Field, datetime.datetime):
    '''
    An abstract Field subclass representing a datetime.
    '''

    __slots__ = ()

    def __new__(cls, year, month, day, hour=0, minute=0, second=0,
                microsecond=0, tzinfo=datetime.timezone.utc):
        return super().__new__(
            cls, year, month, day, hour, minute, second, microsecond=0,
            tzinfo=tzinfo
        )

    @classmethod
    def from_datetime(cls, dt):
        '''
        Create a TimeField instance from a datetime.datetime object.
        If the parameter is naive datetime object, then UTC is assumed,
        otherwise it uses the same timezone of the parameter.

        :type dt: datetime.datetime
        :rtype: TimeField
        '''

        if dt.tzinfo is None:
            dt = dt.replace(tzinfo=datetime.timezone.utc)
        return cls(
            dt.year, dt.month, dt.day, dt.hour, dt.minute, dt.second,
            tzinfo=dt.tzinfo
        )

    @classmethod
    def from_bytes(cls, raw):
        timestamp = int.from_bytes(raw, byteorder='little')
        dt = cls.fromtimestamp(timestamp, tz=datetime.timezone.utc)
        return cls.from_datetime(dt)

    @classmethod
    def now(cls, tz=None):
        '''
        Override datetime.now() to be sure to return TimeField objects
        and not datetime objects.
        '''

        if tz is None:
            tz = datetime.timezone.utc
        return cls.from_datetime(super().now(tz))

    @classmethod
    def utcnow(cls):
        '''
        Override datetime.utcnow() to be sure to return TimeField
        objects and not datetime objects.
        '''

        return cls.now()

    def to_bytes(self):
        utc_self = self.astimezone(datetime.timezone.utc)
        return int(utc_self.timestamp()).to_bytes(4, byteorder='little')

    def __repr__(self):
        return datetime.datetime.__repr__(self)


class IntField(Field, int):
    '''
    An abstract Field subclass representing an integer.
    '''

    __slots__ = ()

    @classmethod
    def _subclass(cls, name, type_id, size):
        T = super()._subclass(name, type_id)
        T.size = size
        return T

    @classmethod
    def from_bytes(cls, raw):
        return cls(int.from_bytes(raw, byteorder='little'))

    def to_bytes(self):
        return int(self).to_bytes(self.size, byteorder='little')


class UUID(Field, uuid.UUID):
    '''
    A concrete field representing an UUID.
    '''

    __slots__ = ()
    type_id = 0x01

    @classmethod
    def new(cls):
        '''
        Create a new instance with a random value.

        :rtype: UUID
        '''

        return cls(bytes=os.urandom(16), version=4)

    @classmethod
    def from_bytes(cls, raw):
        return cls(bytes_le=raw)

    def to_bytes(self):
        return self.bytes_le

    def __repr__(self):
        return uuid.UUID.__repr__(self)


class RawField(Field, bytes):
    '''
    An abstract Field with raw data (bytes).
    '''

    __slots__ = ()

    @classmethod
    def from_bytes(cls, raw):
        return cls(raw)

    def to_bytes(self):
        return self


class End(Field):
    '''
    The last field on a record marking the end of the record.
    '''

    __slots__ = ()
    type_id = 0xff

    @classmethod
    def from_bytes(cls, raw):
        return END

    def to_bytes(self):
        return b''

    def __repr__(self):
        return 'End()'


Version = IntField._subclass('Version', 0x00, 2)
NonDefaultPreferences = TextField._subclass('NonDefaultPreferences', 0x02)
TreeDisplayStatus = TextField._subclass('TreeDisplayStatus', 0x03)
LastSave = TimeField._subclass('LastSave', 0x04)
WhoLastSave = TextField._subclass('WhoLastSave', 0x05)
WhatLastSave = TextField._subclass('WhatLastSave', 0x06)
LastSavedByUser = TextField._subclass('LastSavedByUser', 0x07)
LastSavedOnHost = TextField._subclass('LastSavedOnHost', 0x08)
DatabaseName = TextField._subclass('DatabaseName', 0x0a)
DatabaseDescription = TextField._subclass('DatabaseDescription', 0x0a)
DatabaseFilters = TextField._subclass('DatabaseFilters', 0x0b)
# 0x0c to 0x0e are reserved
RecentlyUsedEntries = TextField._subclass('RecentlyUsedEntries', 0x0f)
NamedPasswordPolicies = TextField._subclass('NamedPasswordPolicies', 0x10)
EmptyGroups = TextField._subclass('EmptyGroups', 0x11)
Yubico = TextField._subclass('Yubico', 0x12)
LastMasterPasswordChange = TimeField._subclass(
    'LastMasterPasswordChange', 0x13
)
END = End()
# record fields
Group = TextField._subclass('Group', 0x02)
Title = TextField._subclass('Title', 0x03)
Username = TextField._subclass('Username', 0x04)
Note = TextField._subclass('Note', 0x05)
Password = TextField._subclass('Password', 0x06)
CreationTime = TimeField._subclass('CreationTime', 0x07)
PasswordModificationTime = TimeField._subclass(
    'PasswordModificationTime', 0x08
)
LastAccessTime = TimeField._subclass('LastAccessTime', 0x09)
PasswordExpiryTime = TimeField._subclass('PasswordExpiryTime', 0x0a)
# _Reserved = IntField._subclass('_Reserved', 0x0b, 4)
LastModificationTime = TimeField._subclass('LastModificationTime', 0x0c)
Url = TextField._subclass('Url', 0x0d)
Autotype = TextField._subclass('Autotype', 0x0e)
PasswordHistory = TextField._subclass('PasswordHistory', 0x0f)
PasswordPolicy = TextField._subclass('PasswordPolicy', 0x10)
PasswordExpiryInterval = IntField._subclass('PasswordExpiryInterval', 0x11, 4)
RunCommand = TextField._subclass('RunCommand', 0x12)
DoubleClickAction = IntField._subclass('DoubleClickAction', 0x13, 2)
EmailAddress = TextField._subclass('EmailAddress', 0x14)
ProtectedEntry = IntField._subclass('ProtectedEntry', 0x15, 1)
OwnSymbolsPassword = TextField._subclass('OwnSymbolsPassword', 0x16)
ShiftDoubleClickAction = IntField._subclass('ShiftDoubleClickAction', 0x17, 2)
PasswordPolicyName = TextField._subclass('PasswordPolicyName', 0x18)
EntryKeyboardShortcut = IntField._subclass('EntryKeyboardShortcut', 0x19, 4)
# _Reserved = UUID._subclass('_Reserved', 0x1a)
TwoFactorKey = RawField._subclass('TwoFactorKey', 0x1b)
CreditCardNumber = TextField._subclass('CreditCardNumber', 0x1c)
CreditCardExpiration = TextField._subclass('CreditCardExpiration', 0x1d)
CreditCardVerification = TextField._subclass('CreditCardVerification', 0x1e)
CreditCardPin = TextField._subclass('CreditCardPin', 0x1f)
QRCode = TextField._subclass('QRCode', 0x20)
# Unknown = RawField._subclass('Unknown', 0xdf)

_HEADER_TYPES = (
    Version, UUID, NonDefaultPreferences, TreeDisplayStatus, LastSave,
    WhoLastSave, WhatLastSave, LastSavedByUser, LastSavedOnHost, DatabaseName,
    DatabaseDescription, DatabaseFilters, RecentlyUsedEntries,
    NamedPasswordPolicies, EmptyGroups, Yubico, End
)
_RECORD_TYPES = (
    UUID, Group, Title, Username, Note, Password, CreationTime,
    PasswordModificationTime, LastAccessTime, PasswordExpiryTime,
    LastModificationTime, Url, Autotype, PasswordHistory, PasswordPolicy,
    PasswordExpiryInterval, RunCommand, DoubleClickAction, EmailAddress,
    ProtectedEntry, OwnSymbolsPassword, ShiftDoubleClickAction,
    EntryKeyboardShortcut, TwoFactorKey, CreditCardNumber,
    CreditCardExpiration, CreditCardVerification, CreditCardPin, QRCode,
    End
)
_HEADERS_MAP = {f.type_id: f for f in _HEADER_TYPES}
_RECORDS_MAP = {f.type_id: f for f in _RECORD_TYPES}
