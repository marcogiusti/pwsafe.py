=========
pwsafe.py
=========
Python3 library to manipulate PasswordSafe V3 files
---------------------------------------------------

Installation
============

To install ``pwsafe.py`` use pip as usual. Note that ``pwsafe.py``
depends on the twofish_ library.

.. code:: shell

   pip install https://github.com/marcogiusti/pwsafe.py

.. _twofish: https://pypi.org/project/twofish/


Usage
=====

To write to a file, create a new PwsafeV3Writer with a file object and
the passphrase and start writing records with the ``write_record``
method. The V3 format file need an header and a series of records.

.. code:: python

   >>> import io
   >>> import pwsafe
   >>> fp = io.BytesIO()
   >>> db_password = b'secret'
   >>> writer = pwsafe.PwsafeV3Writer(fp, db_password)
   >>> header = [pwsafe.Version(3), pwsafe.UUID.new()]
   >>> writer.write_record(header)
   >>> uuid = pwsafe.UUID.new()
   >>> title = pwsafe.Title('Github account')
   >>> url = pwsafe.Url('https://github.com')
   >>> username = pwsafe.Username('marcogiusti')
   >>> password = pwsafe.Password('qwerty')
   >>> record = [uuid, title, username, password]
   >>> writer.write_record(record)
   >>> writer.close()

``PwsafeV3Writer`` does not explicitely close file object it receaved,
remember to do it yourself.

To read from a file, use the ``PwsafeV3Reader``. Let's reuse the file we
already created.

.. code:: python

   >>> fp.seek(0)
   0
   >>> reader = pwsafe.PwsafeV3Reader(fp, db_password)
   >>> header_read, *records = list(reader)
   >>> header_read == header
   True
   >>> records == [record]
   True

Examples
========

.. code:: python

   def record_map(record):
       '''
       Build a map from a record to easily lookup for fields in the
       record.

       Ex.
           record_map(record)[pwsafe.Password]
           Password('qwerty')

       '''

       return {type(f): f for f in record}


   def check_record(record):
       '''
       Check that a record has all the mandatory fields.
       '''

       m = record_map(record)
       return (pwsafe.UUID in m and pwsafe.Title in m and
               pwsafe.Password in m)


   def find(record, field_type, default=None):
       '''
       Search for a specific field or return default.
       '''

       for field in record:
           if isinstance(field, field_type):
               return field
       return default


   def index(db, key):
       '''
       Build an index for the database. The key is a function computing
       a key value for each element. All the records for which the key
       function returns ``None`` are in a list apart. The result of the
       key function must be hashable.

       Ex.
           from functools import partial
           from pwsafe import UUID

           idx = index(db, key=partial(find, field_type=UUID))
       '''

       idx = {}
       others = []
       for record in db:
           k = key(record)
           if k is not None:
               idx[k] = record
           else:
               others.append(record)
       idx[None] = others
       return idx


.. vim: ft=rst tw=72
